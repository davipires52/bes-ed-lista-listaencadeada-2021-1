package br.ucsal.Ed.Lista.questao02;

public class ListaEncadeadaDeAlunos {

	
	private Elemento primeiro;

	private Elemento ultimo;

	private int tamanho;

	public Elemento getPrimeiro() {
		return primeiro;
	}

	public void setPrimeiro(Elemento primeiro) {
		this.primeiro = primeiro;
	}

	public Elemento getUltimo() {
		return ultimo;
	}

	public void setUltimo(Elemento ultimo) {
		this.ultimo = ultimo;
	}

	public int getTamanho() {
		return tamanho;
	}

	public void setTamanho(int tamanho) {

		this.tamanho = tamanho;
	}
	
	/*--------------------------------------------------------*/
	
	public void adicionarAluno(String novoValor) {

		Elemento novoElemento = new Elemento();

		novoElemento.setAluno(novoValor);

		if (primeiro == null && ultimo == null) {
			this.primeiro = novoElemento;
			this.ultimo = novoElemento;
		} else {
			this.ultimo.setProximo(novoElemento);
			this.ultimo = novoElemento;
		}
		this.tamanho++;
	}
	
	public void removerAluno(String valorProcurado) {
		Elemento anterior = null;
		Elemento atual = this.primeiro;
		for (int i = 0; i < this.getTamanho(); i++) {
			if (atual.getAluno().equalsIgnoreCase(valorProcurado)) {
				if (this.tamanho == 1) {
					this.primeiro = null;
					this.ultimo = null;
				} else if (atual == primeiro) {
					this.primeiro = atual.getProximo();
					atual.setProximo(null);
				} else if (atual == ultimo) {
					this.ultimo = anterior;
					anterior.setProximo(null);
				} else {
					anterior.setProximo(atual.getProximo());
					atual = null;
				}

				this.tamanho--;
				break;
			}
			anterior = atual;
			atual = atual.getProximo();

		}
	}
	
	public void listarAlunos() {
		Elemento atual = this.primeiro;
		if (this.getTamanho() == 0) {
			System.out.println("Lista Vazia");
		} else {
			for (int i = 0; i < this.getTamanho(); i++) {
				System.out.println(atual.getAluno());
				if (atual.getAluno() != null) {
					atual = atual.getProximo();
				}

			}
		}

	}
	
	public void organizarOrdemAlfabética() {
		
	}
	
	public Elemento buscarAluno(int posicao) {

		Elemento atual = this.primeiro;

		for (int i = 0; i < posicao; i++) {
			if (atual.getProximo() != null) {
				atual = atual.getProximo();
			}
		}
		return atual;
	}
	
	
}
