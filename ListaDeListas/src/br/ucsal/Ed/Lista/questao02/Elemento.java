package br.ucsal.Ed.Lista.questao02;

public class Elemento {
	
	private String aluno;
	
	private Elemento proximo;

	public String getAluno() {
		return aluno;
	}

	public void setAluno(String aluno) {
		this.aluno = aluno;
	}

	public Elemento getProximo() {
		return proximo;
	}

	public void setProximo(Elemento proximo) {
		this.proximo = proximo;
	}

	
	
}
