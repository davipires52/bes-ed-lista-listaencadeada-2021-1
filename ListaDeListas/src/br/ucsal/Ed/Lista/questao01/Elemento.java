package br.ucsal.Ed.Lista.questao01;

public class Elemento {

	private String professor;
	
	private Elemento proximo;

	public String getProfessor() {
		return professor;
	}

	public void setProfessor(String professor) {
		this.professor = professor;
	}

	public Elemento getProximo() {
		return proximo;
	}

	public void setProximo(Elemento proximo) {
		this.proximo = proximo;
	}
	
	
}
