package br.ucsal.Ed.Lista.questao01;

public class TesteLista {
	
	public static void main(String[] args) {
		ListaEncadeadaDeProfessores lista = new ListaEncadeadaDeProfessores();
		
		lista.adicionar("Angela");
		lista.adicionar("Oswaldo");
		lista.adicionar("Neiva");
		lista.adicionar("Fernando");
		lista.adicionar("Mario");
		
		System.out.println(lista.verificarVazia());
		System.out.println(lista.getTamanho());
		lista.mostrarTodos();
		
		System.out.println("\n");
		
		lista.remover("Neiva");
		lista.remover("Mario");
		lista.remover("Angela");
		System.out.println(lista.getTamanho());
		lista.mostrarTodos();
		
		System.out.println("\n");
		lista.clear();
		lista.adicionar("Pedro");
		lista.mostrarTodos();
	}
	
}
