package br.ucsal.Ed.Lista.questao01;

public class ListaEncadeadaDeProfessores {

	private Elemento primeiro;

	private Elemento ultimo;

	private int tamanho;

	public Elemento getPrimeiro() {
		return primeiro;
	}

	public void setPrimeiro(Elemento primeiro) {
		this.primeiro = primeiro;
	}

	public Elemento getUltimo() {
		return ultimo;
	}

	public void setUltimo(Elemento ultimo) {
		this.ultimo = ultimo;
	}

	public int getTamanho() {
		return tamanho;
	}

	public void setTamanho(int tamanho) {
		this.tamanho = tamanho;
	}

	public ListaEncadeadaDeProfessores() {
		this.tamanho = 0;
	}

	public void adicionar(String novoValor) {

		Elemento novoElemento = new Elemento();

		novoElemento.setProfessor(novoValor);

		if (primeiro == null && ultimo == null) {
			this.primeiro = novoElemento;
			this.ultimo = novoElemento;
		} else {
			this.ultimo.setProximo(novoElemento);
			this.ultimo = novoElemento;
		}
		this.tamanho++;
	}

	public void remover(String valorProcurado) {
		Elemento anterior = null;
		Elemento atual = this.primeiro;
		for (int i = 0; i < this.getTamanho(); i++) {
			if (atual.getProfessor().equalsIgnoreCase(valorProcurado)) {
				if (this.tamanho == 1) {
					this.primeiro = null;
					this.ultimo = null;
				} else if (atual == primeiro) {
					this.primeiro = atual.getProximo();
					atual.setProximo(null);
				} else if (atual == ultimo) {
					this.ultimo = anterior;
					anterior.setProximo(null);
				} else {
					anterior.setProximo(atual.getProximo());
					atual = null;
				}

				this.tamanho--;
				break;
			}
			anterior = atual;
			atual = atual.getProximo();

		}
	}

	public int verificarTamanho() {
		Elemento atual = this.primeiro;
		int cont = 0;
		for (int i = 0; i < this.getTamanho(); i++) {
			if (atual.getProfessor() != null) {
				cont++;
			}
		}
		return cont;
	}

	public boolean verificarVazia() {
		return this.tamanho == 0 ? true : false;
	}

	public void clear() {
		primeiro = null;	
		this.tamanho = 0;
	}

	public void mostrarTodos() {
		Elemento atual = this.primeiro;
		if (this.getTamanho() == 0) {
			System.out.println("Lista Vazia");
		} else {
			for (int i = 0; i < this.getTamanho(); i++) {
				System.out.println(atual.getProfessor());
				if (atual.getProfessor() != null) {
					atual = atual.getProximo();
				}

			}
		}

	}

}
