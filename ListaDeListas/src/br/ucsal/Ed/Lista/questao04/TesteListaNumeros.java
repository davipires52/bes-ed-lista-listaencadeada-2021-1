package br.ucsal.Ed.Lista.questao04;

public class TesteListaNumeros {

	public static void main(String[] args) {

		ListaEncadeadaInteiros lista = new ListaEncadeadaInteiros();
		
		lista.adicionarNumeroInteiro(35);
		lista.adicionarNumeroInteiro(3);
		lista.adicionarNumeroInteiro(19);
		lista.adicionarNumeroInteiro(66);
		lista.adicionarNumeroInteiro(44);
		
		lista.listarNumeros();
		
		System.out.println("\n");
		
		lista.removerNumeroInteiro(19);
		lista.listarNumeros();
		System.out.println("O numero se encontra na posição: " + lista.buscarPosicaoNumeroInteiro(35));
		System.out.println("O primeiro número par é: " + lista.buscarPrimeiroPar());
	}

}
