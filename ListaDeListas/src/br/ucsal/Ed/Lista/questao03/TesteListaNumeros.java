package br.ucsal.Ed.Lista.questao03;

public class TesteListaNumeros {

	public static void main(String[] args) {

		ListaEncadeadaInteiros lista = new ListaEncadeadaInteiros();
		
		lista.adicionarNumeroInteiro(35);
		lista.adicionarNumeroInteiro(2);
		lista.adicionarNumeroInteiro(19);
		lista.adicionarNumeroInteiro(81);
		lista.adicionarNumeroInteiro(44);
		
		lista.listarNumeros();
		
		System.out.println("\n");
		
		lista.removerNumeroInteiro(19);
		lista.listarNumeros();
		System.out.println("O numero se encontra na posição: " + lista.buscarNumeroInteiro(2));
	}

}
