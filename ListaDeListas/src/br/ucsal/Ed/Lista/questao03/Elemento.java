package br.ucsal.Ed.Lista.questao03;

public class Elemento {

	private Integer inteiro;
	
	private Elemento proximo;

	public int getInteiro() {
		return inteiro;
	}

	public void setInteiro(Integer inteiro) {
		this.inteiro = inteiro;
	}

	public Elemento getProximo() {
		return proximo;
	}

	public void setProximo(Elemento proximo) {
		this.proximo = proximo;
	}
	
	
	
}
