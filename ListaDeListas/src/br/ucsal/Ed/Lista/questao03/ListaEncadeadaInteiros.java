package br.ucsal.Ed.Lista.questao03;

public class ListaEncadeadaInteiros {

	private Elemento primeiro;

	private Elemento ultimo;

	private int tamanho;

	public Elemento getPrimeiro() {
		return primeiro;
	}

	public void setPrimeiro(Elemento primeiro) {
		this.primeiro = primeiro;
	}

	public Elemento getUltimo() {
		return ultimo;
	}

	public void setUltimo(Elemento ultimo) {
		this.ultimo = ultimo;
	}

	public int getTamanho() {
		return tamanho;
	}

	public void setTamanho(Integer tamanho) {
		this.tamanho = tamanho;
	}
	
	/*-------------------------------------------------------*/
	
	public void adicionarNumeroInteiro (Integer novoNumero) {

		Elemento novoElemento = new Elemento();

		novoElemento.setInteiro(novoNumero);

		if (primeiro == null && ultimo == null) {
			this.primeiro = novoElemento;
			this.ultimo = novoElemento;
		} else {
			this.ultimo.setProximo(novoElemento);
			this.ultimo = novoElemento;
		}
		this.tamanho++;
	}
	
	public void removerNumeroInteiro(Integer novoNumero) {
		Elemento anterior = null;
		Elemento atual = this.primeiro;
		for (int i = 0; i < this.getTamanho(); i++) {
			if (atual.getInteiro() == novoNumero) {
				if (this.tamanho == 1) {
					this.primeiro = null;
					this.ultimo = null;
				} else if (atual == primeiro) {
					this.primeiro = atual.getProximo();
					atual.setProximo(null);
				} else if (atual == ultimo) {
					this.ultimo = anterior;
					anterior.setProximo(null);
				} else {
					anterior.setProximo(atual.getProximo());
					atual = null;
				}

				this.tamanho--;
				break;
			}
			anterior = atual;
			atual = atual.getProximo();

		}
	}

	public void listarNumeros() {
		Elemento atual = this.primeiro;
		if (this.getTamanho() == 0) {
			System.out.println("Lista Vazia");
		} else {
			for (int i = 0; i < this.getTamanho(); i++) {
				System.out.println(atual.getInteiro());
				if (atual.getInteiro() != 0) {
					atual = atual.getProximo();
				}

			}
		}

	}

	public int buscarNumeroInteiro(Integer inteiroDesejado) {

		Elemento atual = this.primeiro;
		int cont = 1;
		
		while(atual.getInteiro() != inteiroDesejado) {
			cont++;
			atual = atual.getProximo();
		}
		return cont;
	}
}
